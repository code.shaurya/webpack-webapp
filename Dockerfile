FROM node:8.1.3 
 
WORKDIR /app 
 
ADD package.json /app 
ADD yarn.lock /app 
 
RUN yarn install 
 
ADD . /app 
 
EXPOSE 9000 
 
ENV HOST 0.0.0.0 
ENV PORT 9000 
