const path = require('path'); 
const webpack = require('webpack'); 
const HtmlWebpackPlugin = require('html-webpack-plugin'); 
 
const HOST = process.env.HOST; 
const PORT = process.env.PORT; 
 
module.exports = { 
  entry: { 
    app: [ 
      'react-hot-loader/patch', 
      'webpack-dev-server/client?http://' + HOST + ':' + PORT + '/', 
      'webpack/hot/only-dev-server', 
      './src/index.js', 
    ], 
  }, 
  output: { 
    path: path.resolve(__dirname, 'public'), 
    publicPath: "/public/", 
    pathinfo: true, 
    filename: "index.js", 
  }, 
  context: path.resolve(__dirname), 
  devtool: 'inline-source-map', 
  devServer: { 
    hot: true, 
    host: HOST, 
    port: PORT, 
    // proxy: { 
    //   '*': 'http://localhost:6000' 
    // }, 
    historyApiFallback: true, 
    contentBase: path.resolve(__dirname), 
    publicPath: '/public/', 
  }, 
  module: { 
    rules: [ 
      { 
        test: /(\.js)$/, 
        exclude: /(node_modules)/, 
        use: { 
          loader: "babel-loader", 
          options: { 
            presets: ['es2015', 'stage-0', 'react'], 
            cacheDirectory: true, 
          } 
        } 
      } 
    ], 
  }, 
  plugins : [ 
    new webpack.DefinePlugin({ 
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV), 
    }), 
 
    // enable HMR globally 
    new webpack.HotModuleReplacementPlugin(), 
  ] 
}; 