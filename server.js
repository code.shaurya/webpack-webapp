const Server = require('hapi').Server;

const server = new Server();

server.connection({
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 80,
});

const plugins = [
  {
    register: require('inert'),
    options: {}
  },
  {
    register: require('good'),
    options: {
      ops: {
        interval: 1000,
      },
      reporters: {
        console: [{
          module: 'good-squeeze',
          name: 'Squeeze',
          args: [{ log: '*', response: '*' }],
        }, {
          module: 'good-console',
        }, 'stdout'],
      },
    },
  }
];

const routes = [
  {
    method: 'GET',
    path: '/',
    config: {
      handler: function (request, reply) {
        return reply('Welcome.');
        // reply.file('./index.html'); 
      },
    },
  },
  {
    method: 'GET',
    path: '/public/{url*}',
    handler: {
      directory: {
        path: 'public'
      }
    },
  },
  {
    method: 'GET',
    path: '/{url*}',
    handler: function (request, reply) {
      reply.file('./index.html');
    }
  },
]

server.register(plugins, function (err) {
  if (err) {
    throw err;
  }
  else {
    server.route(routes);

    server.start(function (err) {
      if (err) {
        console.log(err);
      }
      else {
        console.log(`Server is running @${server.info.uri}`);
      }
    });
  }
});